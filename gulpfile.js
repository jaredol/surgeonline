var gulp = require('gulp');
var livereload = require('gulp-livereload')
//var uglify = require('gulp-uglifyjs');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat'); 
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var wait = require('gulp-wait');



gulp.task('imagemin', function () {
    return gulp.src('./source/images/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('./source/images/'));
});

// Sass compilation
gulp.task('sass', function () {
  gulp.src('./source/stylesheets/**/*.scss')
    .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(autoprefixer())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./source/stylesheets'));
});

// JS concatenation 
//var jsFiles = './source/js/*.js', 
var jsFiles = ['./source/js/jquery.min.js', './source/js/bootstrap.min.js', './source/js/main.js', './source/js/jquery.validate.js'],  
    jsDest = './source/js';

gulp.task('scripts', function() {  
    return gulp.src(jsFiles)
        .pipe(concat('compiled.js'))
        .pipe(gulp.dest(jsDest));
});


gulp.task('watch', function(){
    livereload.listen();

    gulp.watch('./source/stylesheets/**/*.scss', ['sass']);
    gulp.watch(['./source/stylesheets/style.css', './source/javascripts/*.js'], function (files){
        livereload.changed(files);
    });

    gulp.watch(['./source/**/*.erb'], function (e){
        gulp.src(e.path)
        .pipe(wait(1000))
        .pipe(livereload({ reloadPage: "http://localhost:4567" }));
    });
});
