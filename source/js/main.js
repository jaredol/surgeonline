$(document).ready(function(){

	// Mobile Nav - open / close:
	$(".btn-mobile-hamburger a").on("click", function(e){
		
		// Scroll to top:
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	        var target = $(this.hash);
	        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	        if (target.length) {
	            $('html, body').animate({
	            scrollTop: target.offset().top
	            }, 1000);
				
				// Toggles the slide-in nav:
	           	$(this).parent().toggleClass("closed");
				$(this).parent().find(".fa-bars").addClass("fa-times");
				$(".primary-nav").toggleClass("active");
	            return false;
	        }
	    }

	});



	// Login Form:
	$("#login-form").validate({
	    rules: {
	        email: {
	            required: true
	        },
	        password: {
	            required: true
	        }
	    },
	    messages: {
	        password: {
	            required: "Please provide a password"
	        },
	        email: "Please complete this field"
	    },
	    submitHandler: function(form) {
	        form.submit();
	    }

    }); // end login-form


    // Reset Form:
	$("#reset-form").validate({
		rules: {
			emailaddress: {
				required: true,
				email: true
			}
		},
		messages: {
			email: "Please enter a valid email address"
		},

		submitHandler: function(form) {
			$("#btn-reset").addClass("btn-disabled");  //  Note: this is just temporary code to demonstrate frontend functionality
			$("#reset-form .loader-icon").show();
			//form.submit();
		}
	}); // end reset-form


	// Reset form - Enable / Disable submit button:

	$('#emailaddress').keypress(function () {
        if ($(this).val() === '') {
			$("#btn-reset").attr("disabled", "disabled");
			$("#btn-reset").addClass("btn-disabled");
        } else {
			$("#btn-reset").removeAttr("disabled");
			$("#btn-reset").removeClass("btn-disabled");
        }
    });

	$("#emailaddress").blur(function () {   // when user tabs out of field
		if ($(this).val() === '') {
			$("#btn-reset").attr("disabled", "disabled");
			$("#btn-reset").addClass("btn-disabled");
		} else {
			$("#btn-reset").removeAttr("disabled");
			$("#btn-reset").removeClass("btn-disabled");
		}
	});

	$('.panel-title').on('click', function(){
		$(this).next().toggleClass('hidden');
	});


}); // end doc ready



// Smooth scrolling (applied to links with class of 'back-to-top')

$(function() {

	$('a.back-to-top').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	        var target = $(this.hash);
	        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	        if (target.length) {
	            $('html, body').animate({
	            scrollTop: target.offset().top
	            }, 1000);
	            return false;
	        }
	    }
	});

});  // doc ready



//Closes the main navigation if clicked outside

$(document).on('click', function(event) {
	console.log('first');
	if (!$(event.target).closest('.btn-mobile-hamburger').length) {
	   $('.primary-nav').removeClass('active');
	   $('.btn-mobile-hamburger').removeClass('closed');
	}
});


// Closes the main navigation if clicked outside  (iOS version)

$(document).on('click touchstart','main',function(){
      if (!$(event.target).closest('.btn-mobile-hamburger').length) {
	   $('.primary-nav').removeClass('active');
	   $('.btn-mobile-hamburger').removeClass('closed');
	}
});

// iOS fix, for when mobile panel remains visible after returning to a page using back button

$('.primary-nav a').on('click', function() {
	$('.primary-nav').hide();
});




